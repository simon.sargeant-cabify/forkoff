FROM alpine:3.8

RUN apk --no-cache add ca-certificates=20171114-r3 libc6-compat=1.1.19-r10

COPY forkoff /
 
ENTRYPOINT [ "/forkoff" ]
