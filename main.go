package main

import (
	"log"
	"os"

	gitlab "github.com/xanzy/go-gitlab"
)

func main() {
	args := ParseArgs()

	c := gitlab.NewClient(nil, args.GitlabToken())
	c.SetBaseURL(args.GitlabURL())

	var maintainers []maintainerID

	for _, maintainer := range args.Maintainers() {
		log.Printf("Loading user %s", maintainer)

		u, _, err := c.Users.ListUsers(&gitlab.ListUsersOptions{
			Username: &maintainer,
		})
		if err != nil {
			log.Fatalf("failed while validating user %s: %s", maintainer, err)
		}
		if len(u) != 1 {
			log.Fatalf("invalid list of users for %s: %v", maintainer, u)
		}
		maintainers = append(maintainers, maintainerID{
			ID:   &u[0].ID,
			Name: maintainer})
	}

	log.Printf("Loading source repo %s", args.SourceRepo)
	sourceRepo, _, err := c.Projects.GetProject(args.SourceRepo, nil)
	if err != nil {
		log.Fatalf("Invalid source repo %s: %s", args.SourceRepo, err)
	}

	if args.DryRun {
		log.Println("Everything looks fine, quitting due to dryrun execution")
		os.Exit(0)
	}

	log.Printf("Forking repo %s into %s/%s", args.SourceRepo, args.TargetNamespace, args.TargetRepoName)
	var path *string
	if args.TargetRepoName != "" {
		path = &args.TargetRepoName
	}

	p, _, err := c.Projects.ForkProject(sourceRepo.ID, &gitlab.ForkProjectOptions{
		Namespace: &args.TargetNamespace,
		Name:      path,
		Path:      path,
	})
	if err != nil {
		log.Fatalf("Failed to fork repo %s into %s/%s: %s", args.SourceRepo,
			args.TargetNamespace, args.TargetRepoName, err)
	}

	if args.Detach {
		log.Printf("Removing forking relationship from %s", p.PathWithNamespace)
		if _, err := c.Projects.DeleteProjectForkRelation(p.ID); err != nil {
			log.Fatalf("failed to remove fork relationship of project %s: %s", p.NameWithNamespace, err)
		}
	}

	maintainerLevel := gitlab.MaintainerPermissions
	for _, maintainer := range maintainers {
		log.Printf("Adding maintainer %s to %s", maintainer.Name, p.NameWithNamespace)
		if _, _, err := c.ProjectMembers.AddProjectMember(p.ID, &gitlab.AddProjectMemberOptions{
			UserID:      maintainer.ID,
			AccessLevel: &maintainerLevel,
		}); err != nil {
			log.Fatalf("failed to set user as maintainer of %s", p.NameWithNamespace)
		}
	}
}

type maintainerID struct {
	ID   *int
	Name string
}
