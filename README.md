# Fork OFF

Given a source gitlab repo, it forks it to a new place. Additionally it
supports adding people to it and breaking the forking relationship.

## Usage

```sh
forkoff [options] source-namespace/repo target-namespace
```

### Options

* **-maintainers usernames...** list of users to add as maintainer of the
  forked repo
* **-detach** detach the forking relationship of the forked repo
* **-dryrun** don't actually do anything, just things exist
* **-target-repo** name of the target repository
* **-version** prints the executable version and exits

### Environment variables

* **GITLAB_TOKEN** the user token to use to identify to the gitlab instance
* **GITLAB_BASEURL** the base url of the gitlab instance.
  <https://gitlab.com/api/v4> by default.