package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/yakshaving.art/forkoff/version"
)

const GITLAB_DEFAULT_URL = "http://gitlab.com/api/v4"

func ParseArgs() Args {
	token, ok := os.LookupEnv("GITLAB_TOKEN")
	a := Args{
		gitlabToken: token,
		gitlabURL:   os.Getenv("GITLAB_BASEURL"),
	}

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Fprintf(flag.CommandLine.Output(), "\nEnvironment variables:\n"+
			"  GITLAB_TOKEN the user token used to interact with gitlab - it's required\n"+
			"  GITLAB_BASEURL the api url to interact, "+GITLAB_DEFAULT_URL+" by default\n")
	}

	flag.StringVar(&a.maintainers, "maintainers", "", "maintainers to add to the forked repo, comma separated")
	flag.BoolVar(&a.Detach, "detach", false, "remove the forking relationship after forking")
	flag.BoolVar(&a.Version, "version", false, "show version and exit")
	flag.BoolVar(&a.DryRun, "dryrun", false, "don't actually do anything, just check things exist")
	flag.StringVar(&a.TargetRepoName, "target-repo", "", "path of the forked repo within the target namespace")

	flag.Parse()

	if a.Version {
		log.Println(version.GetVersion())
		os.Exit(0)
	}

	if !ok {
		log.Fatalf("GITLAB_TOKEN environment variable is mandatory")
	}

	if len(flag.Args()) != 2 {
		log.Fatalf("Invalid arguments '%s', expected source and target", flag.Args())
	}
	a.SourceRepo = flag.Arg(0)
	a.TargetNamespace = flag.Arg(1)

	return a
}

type Args struct {
	maintainers    string
	Detach         bool
	TargetRepoName string

	gitlabToken string
	gitlabURL   string

	SourceRepo      string
	TargetNamespace string

	Version bool
	DryRun  bool
}

func (a Args) Maintainers() []string {
	if strings.TrimSpace(a.maintainers) == "" {
		return []string{}
	}
	return strings.Split(a.maintainers, ",")
}

func (a Args) GitlabURL() string {
	if a.gitlabURL == "" {
		return GITLAB_DEFAULT_URL
	}
	return a.gitlabURL
}

func (a Args) GitlabToken() string {
	return a.gitlabToken
}
